import open3d as o3d
import os
from PyInquirer import prompt

def display_inlier_outlier(cloud, ind):
    inlier_cloud = cloud.select_down_sample(ind)
    outlier_cloud = cloud.select_down_sample(ind, invert=True)

    print("outliers - red, inliers - gray")
    outlier_cloud.paint_uniform_color([1, 0, 0])
    inlier_cloud.paint_uniform_color([0.8, 0.8, 0.8])
    o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


def prepare_dataset(cloud, voxel_size):
    print("preparing poincloud")
    point_cloud = o3d.io.read_point_cloud(cloud)
    pcd_down = preprocess_point_cloud(point_cloud, voxel_size)

    return pcd_down


def preprocess_point_cloud(cloud, voxel_size):
    print("downsampling poincloud. voxel size: %.3f." % voxel_size)
    pcd_down = cloud.voxel_down_sample(voxel_size)

    # radius_normal = voxel_size * 2
    # print(":: Estimate normal with search radius %.3f." % radius_normal)
    # pcd_down.estimate_normals(
    #     o3d.geometry.KDTreeSearchParamHybrid(radius=radius_normal, max_nn=30))

    return pcd_down


def statistical_removal(cloud, nb, ratio):
    cropped_cloud, indices = cloud.remove_statistical_outlier(nb_neighbors=nb, std_ratio=ratio)
    print("displaying statistical removal result")
    display_inlier_outlier(cloud, indices)
    return cropped_cloud


def radius_removal(cloud, nb, radius):
    cropped_cloud, indices = cloud.remove_radius_outlier(nb_points=nb, radius=radius)
    print("displaying radius removal result")
    display_inlier_outlier(cloud, indices)
    return cropped_cloud


def deliver_data(dataset):
    currentDirectory = os.getcwd()
    print(currentDirectory)

    if (dataset == {'cloud': '1'}):
        pointcloud = currentDirectory + "\\pointclouds\\noise_beispiel_1.ply"
        return pointcloud
    if (dataset == {'cloud': '2'}):
        pointcloud = currentDirectory + "\\pointclouds\\noise_beispiel_2.ply"
        return pointcloud
    if (dataset == {'cloud': '3'}):
        pointcloud = currentDirectory + "\\pointclouds\\noise_beispiel_3.ply"
        return pointcloud
    if (dataset == {'cloud': '4'}):
        pointcloud = currentDirectory + "\\pointclouds\\noise_beispiel_4.ply"
        return pointcloud
    if (dataset == {'cloud': '5'}):
        pointcloud = currentDirectory + "\\pointclouds\\noise_beispiel_5.ply"
        return pointcloud


def choose_removal_inquire():
    removalQ = [
        {
            'type': 'list',
            'name': 'removal',
            'message': 'Which removal?',
            'choices': [
                'statistical outlier removal',
                'radius outlier removal'
            ]
        }
    ]

    removal = prompt(removalQ)
    return removal


def menu_inquire():
    cloudQ = [
        {
            'type': 'list',
            'name': 'cloud',
            'message': 'Which pointcloud do you want to test?',
            'choices': [
                '1',
                '2',
                '3',
                '4',
                '5'
            ]
        }
    ]

    cloud = prompt(cloudQ)
    return cloud


if __name__ == "__main__":
    dataset = menu_inquire()
    mode = choose_removal_inquire()

    cloud = deliver_data(dataset)
    cloud_down = prepare_dataset(cloud, voxel_size=0.02)

    if (mode == {'removal': 'statistical outlier removal'}):
        prepared_cloud = statistical_removal(cloud_down, 20, 2.0)
    else:
        prepared_cloud = radius_removal(cloud_down, 16, 0.05)

    print('visualizing cropped pointcloud')
    o3d.visualization.draw_geometries([prepared_cloud])
    print('input any key to exit')
    input()